import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Subject} from 'rxjs/Subject';
import {Film} from './film';

@Injectable()
export class FilmService {

  films: any[];
  lastFilms: any[];
  filmSubject: Subject<Film[]> = new Subject<Film[]>();
  lastFilmSubject: Subject<Film[]> = new Subject<Film[]>();

  constructor(private http: HttpClient) {

  }

  getFilms() {

    this.http.get<any[]>('http://127.0.0.1:8000/film/').subscribe(
      (data) => {
        console.log('données reçues: ', data)

        // on transforme les donnees
        let films = [];
        for (let [id, film] of Object.entries(data)) {
          film.id = id
          films.push(film);
        }

        console.log(films)

        // on met a jour films dans le service
        this.films = films
        this.filmSubject.next(this.films);

      },
      // next()
      (error) => console.error('erreur: ', error), // error()
      () => console.log('terminé') // complete()
    );
  }

  // getLastFilms() {
  //   this.http.get<any[]>('http://127.0.0.1:8000/film/last').subscribe(
  //     (data) => {
  //       console.log('données reçues: ', data)
  //
  //       // on transforme les donnees
  //       let lastFilms = [];
  //       for (let [id, film] of Object.entries(data)) {
  //         film.id = id
  //         lastFilms.push(film);
  //       }
  //
  //       console.log(lastFilms)
  //
  //       // on met a jour films dans le service
  //       this.lastFilms = lastFilms
  //       this.lastFilmSubject.next(this.lastFilms);
  //
  //     },
  //     // next()
  //     (error) => console.error('erreur: ', error), // error()
  //     () => console.log('terminé') // complete()
  //   );
  //
  // }

  getLastFilmTest() {
    let LastFilm = [];
    this.films.forEach(function(elem) {
      LastFilm.unshift(elem);
    });

    this.lastFilms = LastFilm.slice(0, 4);
    this.lastFilmSubject.next(this.lastFilms);
  }

}

import {Component, DoCheck, OnChanges, OnInit, ViewChild} from '@angular/core';
import {FilmService} from '../film.service';
import {Film} from '../film';
import {Router} from '@angular/router';
import {OwlCarousel} from 'ngx-owl-carousel';
declare var $: any;

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit, DoCheck {

  @ViewChild('owlElement') owlElement: OwlCarousel;
  constructor(private filmService: FilmService, private router: Router) { }

  public films: Film[] = [];
  public lastFilms: Film[] = [];

  ngDoCheck() {
    this.owlElement.next([30000]);
  }

  ngOnInit() {

    this.filmService.getFilms(); // juste la requete AJAX
    this.filmService.filmSubject.subscribe(
      (films: Film[]) => {
        this.films = films;
        this.filmService.getLastFilmTest();
      }
    );
    this.filmService.lastFilmSubject.subscribe(
      (films: Film[]) => {
        this.lastFilms = films;
        console.log(this.lastFilms);
      }
    );
  }

}

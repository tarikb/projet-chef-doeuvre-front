import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { FilmsComponent } from './films/films.component';
import { FilmComponent } from './film/film.component';
import {LastFilmsComponent} from './last-films/last-films.component';

export const routes: Routes = [
  { path: 'films', component: FilmsComponent },
  { path: 'films', component: LastFilmsComponent},
  { path: 'film/:id', component: FilmComponent},
  { path: '', component: FilmsComponent},

]

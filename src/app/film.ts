export interface Film {
  id:number,
  titre:string,
  acteur:string;
  critique:string;
  dateSortie:string;
  duree:string;
  genre:string;
  image:string;
  nationalite:string;
  realisateur:string;
}

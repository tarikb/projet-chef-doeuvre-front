import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FilmService} from '../film.service';
import {Film} from '../film';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.css']
})
export class FilmComponent implements OnInit {

  public id: string;
  public film;

  constructor(private route: ActivatedRoute, private filmService: FilmService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.filmService.filmSubject.subscribe(
      (films: Film[]) => {
        console.log(films);
        this.film = films.find(film => String(film.id) === this.id);

      }
    );
    this.filmService.getFilms();


    this.id = this.route.snapshot.params['id'];
    this.filmService.lastFilmSubject.subscribe(
      (films: Film[]) => {
        console.log(films);
        this.film = films.find(film => String(film.id) === this.id);

      }
    );
    this.filmService.getLastFilmTest();
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { FilmComponent } from './film/film.component';
import { HttpClientModule } from '@angular/common/http';
import { FilmsComponent } from './films/films.component';
import {FilmService} from './film.service';
import { FilmItemComponent } from './film-item/film-item.component';
import {routes} from './routes';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { OwlModule } from 'ngx-owl-carousel';
import { LastFilmsComponent } from './last-films/last-films.component';



@NgModule({
  declarations: [
    AppComponent,
    FilmComponent,
    FilmsComponent,
    FilmItemComponent,
    NavbarComponent,
    FooterComponent,
    LastFilmsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    OwlModule
  ],
  providers: [
    FilmService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

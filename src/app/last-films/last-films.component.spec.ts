import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastFilmsComponent } from './last-films.component';

describe('LastFilmsComponent', () => {
  let component: LastFilmsComponent;
  let fixture: ComponentFixture<LastFilmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastFilmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastFilmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

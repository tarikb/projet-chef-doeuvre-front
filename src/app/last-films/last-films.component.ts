import {Component, Input, OnInit} from '@angular/core';
import {FilmService} from '../film.service';

@Component({
  selector: 'app-last-films',
  templateUrl: './last-films.component.html',
  styleUrls: ['./last-films.component.css']
})
export class LastFilmsComponent implements OnInit {

  // propriétés
  @Input() public titre: string
  @Input() public acteur: string
  @Input() public critique: string
  @Input() public dateSortie: string
  @Input() public duree: string
  @Input() public genre: string
  @Input() public image: string
  @Input() public nationalite: string
  @Input() public realisateur: string
  @Input() public id: number

  constructor(private filmService: FilmService) {
  }
  ngOnInit() {
  }

}
